'use strict';
const Hapi = require('@hapi/hapi');
const formMetaData = require("./mocks/metadata.json");

const init = async () => {

    const server = Hapi.server({
        port: 3000,
        host: 'localhost'
    });
    server.route(gettingFormMetaData)
    server.route(sendForm);
    await server.start();
    console.log('Server running on %s', server.info.uri);
};


const gettingFormMetaData = {
    method: 'GET',
    path: '/api/v1/form/metadata',
    handler: (request, h) => {
        return formMetaData;
    },
    config: {
        cors: {
            origin: ['*'],
            additionalHeaders: ['cache-control', 'x-requested-with']
        }
    }
}

const sendForm = {
    method: "POST",
    path: "/api/v1/form/send",
    handler: (request, reply) => {
        console.log(request.payload);
        return {
            status: "Success"
        };
    },
    config: {
        cors: {
            origin: ['*'],
            additionalHeaders: ['cache-control', 'x-requested-with']
        }
    }
}

process.on('unhandledRejection', (err) => {
    console.log(err);
    process.exit(1);
});

init();
