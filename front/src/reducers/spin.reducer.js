import {CHANGE_SPIN_VISIBLE} from "./actionTypes";

let initialSpinState = {
    visibleSpin: false
}

export function spin(state = initialSpinState, action) {
    switch (action.type) {
        case CHANGE_SPIN_VISIBLE:
            return {
                ...state,
                visibleSpin: action.visibleSpin
            };
        default:
            return state;
    }
}
