import {CHANGE_BUTTON_ENABLE} from "./actionTypes";

let initialButtonState = {
    isEnabled: true
};

export function buttonSubmit(state = initialButtonState, action) {
    switch (action.type) {
        case CHANGE_BUTTON_ENABLE:
            return {
                ...state,
                isEnabled: action.isEnabled
            };
        default:
            return state;
    }
}
