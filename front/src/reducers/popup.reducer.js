import {HIDE_POPUP, SHOW_POPUP} from "./actionTypes";

export function showPopup(title, status) {
    return {
        type: SHOW_POPUP,
        title: title,
        status: status,
        visible: true
    }
}

export function hidePopup() {
    return {
        type: HIDE_POPUP,
        visible: false
    }
}

export function popup(state = {}, action) {
    switch (action.type) {
        case SHOW_POPUP:
            return {
                ...state,
                status: action.status,
                title: action.title,
                visiblePopup: action.visible
            };
        case HIDE_POPUP:
            return {
                ...state,
                visiblePopup: action.visible
            };
        default:
            return state;
    }
}

