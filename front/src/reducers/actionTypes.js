//POPUP_ACTION
export const SHOW_POPUP = "SHOW_POPUP";
export const HIDE_POPUP = "HIDE_POPUP";
//FORM_ACTION
export const CHANGE_FORM_ITEM = "CHANGE_FORM_ITEM";
export const CLEAN_FORM = "CLEAN_FORM";
//BUTTON_ACTION
export const CHANGE_BUTTON_ENABLE = "CHANGE_BUTTON_ENABLE";
//SPIN_ACTION
export const CHANGE_SPIN_VISIBLE = "CHANGE_SPIN_VISIBLE";
