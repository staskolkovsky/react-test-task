import {CHANGE_FORM_ITEM, CLEAN_FORM} from "./actionTypes";

export function form(state = {}, action) {
    switch (action.type) {
        case CHANGE_FORM_ITEM:
            return {
                ...state,
                [action.fieldName]: action.value
            };
        case CLEAN_FORM:
            return {};
        default:
            return {
                ...state
            };
    }
}

