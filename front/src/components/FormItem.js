import React from "react";
import Label from 'arui-feather/label';
import {Input} from "arui-feather/input";
import Select from 'arui-feather/select';
import EmailInput from 'arui-feather/email-input';
import MoneyInput from 'arui-feather/money-input';
import PhoneInput from 'arui-feather/phone-input';
import {store} from "../index";
import {connect} from "react-redux";
import {LIST_TYPE_INPUT, NUMERIC_TYPE_INPUT} from "../constants/constants";
import {CHANGE_FORM_ITEM} from "../reducers/actionTypes";
import "../styles/FormItem.css";
import "../styles/media.css";

const FormItemComponent = (props) => {
    return (
        <div className="form-item">
            <div className="form-item__label">
                <Label size="l" isNoWrap={false} className="form-item__label-item">
                    {props.field?.title}
                </Label>
            </div>
            <div className="form-item__controller">
                {getInputByType(props)}
            </div>
        </div>
    );
}

const dispatchFormItemChange = (data, props) => {
    store.dispatch({type: CHANGE_FORM_ITEM, fieldName: props.field.name, value: data});
}

function getInputByType(props) {
    const fieldType = props.field.type;
    const fieldName = props.field.name;
    if (fieldType === LIST_TYPE_INPUT) {
        return getSelectInput(props);
    } else if (fieldType === NUMERIC_TYPE_INPUT) {
        return fieldName === "money"
            ? getMoneyInput(props)
            : fieldName === "phoneNumber"
                ? getPhoneNumberInput(props)
                : getTextInput(props, true);
    } else if (fieldName === "email") {
        return getEmailInput(props);
    } else {
        return getTextInput(props, false);
    }
}

const getTextInput = (props, isNumberType) => {
    return (
        <Input
            key={props.field?.name}
            placeholder={'Введите ' + props.field?.title?.toLowerCase()}
            view='line'
            width='available'
            clear={true}
            type={isNumberType ? 'number' : 'text'}
            size='l'
            onChange={(data) => dispatchFormItemChange(data, props)}
        />
    );
}

const getMoneyInput = (props) => {
    return (
        <MoneyInput
            currencyCode='USD'
            onChange={(data) => dispatchFormItemChange(data, props)}
        />
    );
}

const getEmailInput = (props) => {
    return (
        <EmailInput placeholder='Введите e-mail'
                    onChange={(data) => dispatchFormItemChange(data, props)}/>
    );
}

const getPhoneNumberInput = (props) => {
    return (
        <PhoneInput
            placeholder='+375'
            onChange={(data) => dispatchFormItemChange(data, props)}/>
    );
}

const getSelectInput = (props) => {
    const options = props.field.values?.map(valueSelect => {
        return {
            value: valueSelect,
            text: valueSelect
        }
    });

    return (
        <Select
            size='l'
            mode='radio'
            options={options}
        />
    )
}

const mapStateToProps = (state) => {
    return {
        ...state.form
    };
}

export default connect(mapStateToProps)(FormItemComponent);
