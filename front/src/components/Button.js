import React, {Component} from "react";
import Button from 'arui-feather/button';
import "../styles/Button.css";
import {connect} from "react-redux";

class ButtonComponent extends Component {
    constructor(props) {
        super(props);
        this.submitForm = this.submitForm.bind(this);
    }

    submitForm() {
        this.props.sendData();
    }

    render() {
        return (
            <div className="button-container">
                <Button view='extra'
                        size='l'
                        disabled={!this.props.isEnabled}
                        onClick={this.submitForm}>{'Отправить'}</Button>
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        ...state.buttonSubmit
    };
}

export default connect(mapStateToProps)(ButtonComponent);
