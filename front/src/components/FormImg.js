import React from "react";
import "../styles/Image.css";

const FormImgComponent = (props) => {
    return (
        <img src={props.image}/>
    );
}

export default FormImgComponent;
