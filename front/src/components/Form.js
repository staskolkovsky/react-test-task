import React from "react";
import FormItemComponent from "./FormItem";
import "../styles/Form.css";

const FormComponent = (props) => {
    return (
        <div className="form-items">
            {
                props.fields?.map(field => {
                    return <FormItemComponent key={field.name} field={field}  />
                })
            }
        </div>
    );
}

export default FormComponent;
