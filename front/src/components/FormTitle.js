import React from "react";
import Heading from 'arui-feather/heading';

const FormTitleComponent = (props) => {
    return (
        <div>
            <Heading size='m'>
                {props.title}
            </Heading>
        </div>
    );
}

export default FormTitleComponent;
