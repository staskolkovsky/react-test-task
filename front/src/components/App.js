import React, {Component} from "react";
import FormComponent from "./Form";
import FormTitleComponent from "./FormTitle";
import SpinnerComponent from "./Spinner";
import ButtonComponent from "./Button";
import PopupComponent from "./Popup";
import FormImgComponent from "./FormImg";
import {store} from "../index";
import {showPopup} from "../reducers/popup.reducer";
import {CHANGE_BUTTON_ENABLE, CHANGE_SPIN_VISIBLE, CLEAN_FORM} from "../reducers/actionTypes";
import {connect} from "react-redux";
import "../styles/App.css";
import "../styles/media.css";

class AppComponent extends Component {
    constructor(props) {
        super(props);
        this.state = {
            formMetaData: {}
        };
        this.sendData = this.sendData.bind(this);
    }

    componentDidMount() {
        fetch("http://localhost:3000/api/v1/form/metadata")
            .then(response => response.json())
            .then(formMetaData => {
                if (this.state.formMetaData !== formMetaData) {
                    this.setState({formMetaData: formMetaData, visibleSpin: false});
                }
            });
    }

    sendFormToServer() {
        store.dispatch({type: CHANGE_BUTTON_ENABLE, isEnabled: false});
        store.dispatch({type: CHANGE_SPIN_VISIBLE, visibleSpin: true});
        /*Stub waiting response*/
        setTimeout(() => {
            fetch('http://localhost:3000/api/v1/form/send', {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json;charset=utf-8'
                },
                body: JSON.stringify({"form": store.getState().form})
            })
                .then(response => response.json())
                .then(() => {
                    store.dispatch({type: CHANGE_SPIN_VISIBLE, visibleSpin: false});
                    store.dispatch(showPopup("Ваш баланс пополнен", "ok"));
                    store.dispatch({type: CLEAN_FORM});
                    store.dispatch({type: CHANGE_BUTTON_ENABLE, isEnabled: true});
                });
        }, 5000);
    }

    sendData() {
        this.sendFormToServer();
    }

    render() {
        const overlay = <div className="spinner-overlay"></div>;
        return (
            <div className="main-container">
                <SpinnerComponent/>
                {this.props.visibleSpin ? overlay : ""}
                <div className="popup-container">
                    <PopupComponent/>
                </div>
                <div className="form-container">
                    <FormTitleComponent title={this.state.formMetaData?.title}/>
                    <FormComponent fields={this.state.formMetaData?.fields}/>
                    <ButtonComponent sendData={this.sendData}/>
                </div>
                <div className="image-container">
                    <FormImgComponent image={this.state.formMetaData?.image}/>
                </div>
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        ...state.spin
    };
}

export default connect(mapStateToProps)(AppComponent);
