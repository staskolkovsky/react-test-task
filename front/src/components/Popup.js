import React from "react";
import Notification from "arui-feather/notification";
import {connect} from "react-redux";
import {store} from "../index";
import {hidePopup} from "../reducers/popup.reducer";

const PopupComponent = (props) => {
    return (
        <div>
            <Notification
                visible={props.visiblePopup}
                status={props.status}
                offset={12}
                stickTo='left'
                title={props.title}
                onCloserClick={() => closeNotification()}
            >
            </Notification>
        </div>
    );
}

const closeNotification = () => {
    store.dispatch(hidePopup());
}

const mapStateToProps = (state) => {
    return {
        ...state.popup
    };
}

export default connect(mapStateToProps)(PopupComponent);
