import React from "react";
import {connect} from "react-redux";
import Spin from "arui-feather/spin";
import "../styles/Spinner.css";
import Label from 'arui-feather/label';

const SpinnerComponent = (props) => {
    const spinnerElement = (
        <div className="spinner-container">
            <Spin
                size='xl'
                visible={props.visibleSpin}
            />
            <Label size='xl' isNoWrap={ true } className="spinner-container__label">
                Обработка запроса...
            </Label>
        </div>
    );

    return (
        props.visibleSpin ? spinnerElement : ""
    );
};


const mapStateToProps = (state) => {
    return {
        ...state.spin
    };
}

export default connect(mapStateToProps)(SpinnerComponent);

