import React from "react";
import ReactDOM from "react-dom";
import App from "./components/App";
import {Provider} from "react-redux";
import {createStore, combineReducers} from "redux";
import {popup} from "./reducers/popup.reducer";
import {form} from "./reducers/form.reducer";
import {buttonSubmit} from "./reducers/button.reducer";
import {spin} from "./reducers/spin.reducer";

export const store = createStore(combineReducers({form, popup, buttonSubmit, spin}));

ReactDOM.render(
    <Provider store={store}>
        <App/>
    </Provider>,
    document.getElementById("root"));
